import requests, codecs, sys, logging, json, os, csv, time
from locationreporter import LocationReporter

logging.basicConfig(level=logging.INFO)

class CountryReporter:
    """an api library in python for Adzuna stats report calls"""
    
    def __init__(self):
        self.logger = logging.getLogger(__name__)
        # load config
        params_data = open('params.json')
        self.params = json.load(params_data)
        params_data.close()
        # get source file from command line
        self.source = str(os.getcwd()+ "/" + sys.argv[1])

        # prepare results list which will be exported to csv
        self.results = []
        self.months = []
        # open csv and loop through rows containing locations
        with open(self.source,'rb') as source_file:
            reader = csv.reader(source_file)
            for row in reader: 
                counter = 0
                # add URI location names to URI patterns to build call
                for location in row:
                    # (TODO) HERE YOU COULD CHECK FOR NON-ASCII CHARS 
                    self.params['uri']['location'+str(counter)] = location
                    counter += 1
                # create new single location report
                locationresult = LocationReporter(self.params)
                if self.params['historical']:
                    self.months = locationresult.months
                    self.results.append([self.params['uri']['location'+str(counter-1)], locationresult.historycount(), locationresult.historysalary()])
                else:
                    self.results.append([self.params['uri']['location'+str(counter-1)], locationresult.count(), locationresult.salary()])
                
                self.params['uri'].pop('location0', None)
                self.params['uri'].pop('location1', None)
                self.params['uri'].pop('location2', None)
                self.params['uri'].pop('location3', None)
                self.params['uri'].pop('location4', None)
                    
        
    def export(self):
        if self.params['historical']:
            with open(self.params['country'] + ' HISTORICAL ' + time.strftime("%d-%m-%Y %H%M") +'.csv', 'wb') as outputfile:
                writer = csv.writer(outputfile, delimiter=',', quotechar="|", quoting=csv.QUOTE_MINIMAL)
                header = ['Locations','Listings']
                header.extend(self.months)
                header.append('Salaries')
                header.extend(self.months)
                writer.writerow(header)
                for locationresult in self.results:
                    row = [locationresult[0], '']
                    for listings in locationresult[1]:
                        row.append(listings)
                    row.append('')
                    for salaries in locationresult[2]:
                        row.append(salaries)
                    writer.writerow(row)                
        else:
            with open('CURRENT ' + time.strftime("%d-%m-%Y %H%M") +'.csv', 'wb') as outputfile:
                header = ['Locations', 'Listings', 'Average Salary']
                writer = csv.writer(outputfile, delimiter=',', quotechar="|", quoting=csv.QUOTE_MINIMAL)
                writer.writerow(header)
                for locationdata in self.results:
                    writer.writerow([locationdata[0], locationdata[1], locationdata[2]])
        
report = CountryReporter()
report.export()


