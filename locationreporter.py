import requests, codecs, logging, pprint, datetime

logging.basicConfig(level=logging.INFO)

class LocationReporter:
    """an api library in python for Adzuna stats report calls"""
    def __init__(self, params):
        self.logger = logging.getLogger(__name__)
        self.logger.info("Started object for single location")
        self.country = params['country']
        self.historical = params['historical']
        self.uriparams = params['uri']
        self.pages = params.get('pages', 1)
        
        if self.historical:
            self.calltype = 'history'
        else:
            self.calltype = 'search'
        
        try:
            self.calldata = self.call()
        except:
            if self.historical:
                self.calldata = { 'count':[], 'total':[], 'month':{}}
            else:
                self.calldata = { 'count':None, 'total':None, 'month':{}}
        
        if self.historical:    
            # only get past 12 months
            if 'total' in self.calldata:
                # you can extend results by not limiting the months to 12
                self.months = sorted(self.calldata['total'])[-12:]
            else:
                self.months = [datetime.date.today().strftime("%Y-%m")]
        
        
    def call(self):
        uri = ("http://api.adzuna.com:80/v1/api/jobs", self.country, self.calltype) 
        uri = "{0}/{1}/{2}/".format(*uri)
        if self.calltype == "search":
            response = {}
            for i in xrange(1, self.pages +1):
                tmpuri = uri + str(i)
                temp = requests.get(tmpuri, params=self.uriparams).json()
                if i != 1:
                    response['results'].extend(temp['results'])
                else:
                    response = temp
                    i += 1
        else:
            response = requests.get(uri, params=self.uriparams).json()
        #error checking
        if 'exception' in response:
            self.logger.error('API ERROR CODE: ' + response['exception'] + ' REASON:' + response['display'])
            raise RuntimeError
        else:
            # uncomment this for debugging (if you need to find out the exact response)
            self.logger.info(response)
            self.response = response
            return response
        
        
    def count(self):
        if self.historical:
            raise RuntimeError('Only works when historical is False')
        else:
            if 'count' in self.calldata:
                return self.calldata['count']
            
    def salary(self):
        if self.historical:
            raise RuntimeError('Only works when historical is False')
        else:
            if 'mean' in self.calldata:
                return self.calldata['mean']
            
            
    def historycount(self):
        if self.historical:
            listingcount = []
            for month in self.months:
                if month in self.calldata['total']:
                    listingcount.append(self.calldata['total'][month])
            return listingcount
        else:
            raise RuntimeError('Only works on historical setting')
        
            
    def historysalary(self):
        if self.historical:
            salaries = []
            for month in self.months:
                if month in self.calldata['month']:
                    salaries.append(self.calldata['month'][month])
            return salaries
        else:
            raise RuntimeError('Only works on historical setting')
            
        
    def titles(self, filelocation):
        if self.calltype != 'search':
            raise RuntimeError('Can only get titles from a /search query')
        else:
            data = self.calldata['results']
            titles = []
            for i in xrange(0, len(data)):
                titles.append(data[i]['title'])
        
            with codecs.open(filelocation,'w','utf-8') as file:
                print>>file, calldata['count']
                for title in titles:
                    print>>file, title
        
    

params = {'historical':True, 'country':'de', 'calltype':'history', 'uri':{'app_id':'28f6787e', 'app_key':'4dzun4pi','category':'manufacturing-jobs'}}
